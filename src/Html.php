<?php

declare(strict_types=1);

namespace LifePHP\Utils;

class Html
{
    use StaticClass;

    /**
     * This method strips HTML5 tags and rest of HTML characters (';', '&', ...) translate to HTML entities
     *
     * @param  string $html
     * @param  bool   $doubleEncode - [optional]
     * When double_encode is turned off PHP will not encode existing html entities.
     * The default is to convert everything.
     * @return string
     */
    public static function htmlToText(string $html, bool $doubleEncode = true): string
    {
        return htmlentities(strip_tags($html), ENT_QUOTES | ENT_HTML5, 'UTF-8', $doubleEncode);
    }
}
