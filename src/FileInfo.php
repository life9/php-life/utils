<?php

declare(strict_types=1);

namespace LifePHP\Utils;

use LifePHP\Utils\FileSystem;
use SplFileInfo;
use LifePHP\Utils\Exceptions\IOException;

class FileInfo extends SplFileInfo
{
    public function __construct(
        private readonly string $filepath
    ) {
        parent::__construct($filepath);
    }

    /**
     * @throws IOException
     */
    public function read(): string
    {
        $content = @file_get_contents($this->filepath);

        if ($content === false) {
            throw new IOException(
                sprintf(
                    'File cannot be read: %s. %s',
                    FileSystem::normalizePath($this->filepath),
                    ExceptionHelper::getLastError(),
                ),
                1,
            );
        }

        return $content;
    }

    /**
     * @throws IOException
     */
    public function write(string $text, bool $append = false): void
    {
        $dir = dirname($this->filepath);

        FileSystem::createDir($dir);

        $flags = ($append ? FILE_APPEND : 0) | LOCK_EX;

        if (@file_put_contents($this->filepath, $text, $flags) === false) {
            throw new IOException(
                sprintf(
                    'Unable to write into file: %s. %s',
                    FileSystem::normalizePath($this->filepath),
                    ExceptionHelper::getLastError(),
                ),
                2,
            );
        }
    }

    public function getNormalizedExtension(): string
    {
        $extension = $this->getExtension();
        return FileSystem::normalizeFileExtension($extension);
    }

    /**
     * @throws IOException
     */
    public function chmod(int $mode): void
    {
        FileSystem::chmod($this->filepath, $mode);
    }
}
