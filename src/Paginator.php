<?php

declare(strict_types=1);

namespace LifePHP\Utils;

use LifePHP\Utils\Exceptions\InvalidArgumentException;

class Paginator
{
    /** @var positive-int */
    private int $page = 1;

    /** @var positive-int */
    private int $itemsPerPage = 1;

    /** @var positive-int */
    private ?int $itemsPerFirstPage = null;

    private ?int $itemsCount = null;

    public function setPage(int $page): Paginator
    {
        if ($page < 1) {
            throw new InvalidArgumentException('Page must be possitive integer', 001);
        }
        $this->page = $page;
        return $this;
    }

    public function setItemsPerPage(int $itemsPerPage): Paginator
    {
        if ($itemsPerPage < 1) {
            throw new InvalidArgumentException('Items per page must be possitive integer', 002);
        }
        $this->itemsPerPage = $itemsPerPage;
        return $this;
    }

    public function setItemsPerFirstPage(?int $itemsPerFirstPage): Paginator
    {
        if ($itemsPerFirstPage !== null && $itemsPerFirstPage < 1) {
            throw new InvalidArgumentException('Items per first page must be possitive integer', 002);
        }
        $this->itemsPerFirstPage = $itemsPerFirstPage;
        return $this;
    }

    public function setItemsCount(int $itemsCount): Paginator
    {
        if ($itemsCount < 0) {
            throw new InvalidArgumentException('Items count must be possitive integer', 003);
        }
        $this->itemsCount = $itemsCount;
        return $this;
    }

    public function getPage(): int
    {
        return $this->page;
    }

    public function getItemsPerPage(): int
    {
        return $this->itemsPerPage;
    }

    public function getItemsPerFirstPage(): int
    {
        return $this->itemsPerFirstPage ?? $this->itemsPerPage;
    }

    public function getItemsCount(): ?int
    {
        return $this->itemsCount;
    }

    public function getOffset(): int
    {
        if ($this->getActualPage() === 1) {
            return 0;
        }

        return $this->getItemsPerFirstPage() + ($this->getActualPage() - 2) * $this->getItemsPerPage();
    }

    public function getPageCount(): ?int
    {
        if (!$this->getItemsCount()) {
            return $this->getItemsCount();
        }

        $itemsCountWithoutFirstPage = max($this->getItemsCount() - $this->getItemsPerFirstPage(), 0);
        return 1 + (int) ceil($itemsCountWithoutFirstPage / $this->getItemsPerPage());
    }

    public function getActualPage(): int
    {
        $pageCount = $this->getPageCount();

        if (!$pageCount) {
            return 1;
        }

        return min($pageCount, $this->getPage());
    }

    public function getFirstIndexOfPage(): int
    {
        return min($this->getOffset() + 1, (int) $this->getItemsCount());
    }

    public function getLastIndexOfPage(): int
    {
        $itemsOnPage = $this->getItemsCountOnActualPage();
        return min($this->getOffset() + $itemsOnPage, (int) $this->getItemsCount());
    }

    public function isFirst(): bool
    {
        return $this->getActualPage() === 1;
    }

    public function isLast(): bool
    {
        return $this->getPage() >= $this->getPageCount();
    }

    public function getItemsCountOnActualPage(): int
    {
        $count = $this->isFirst() ? $this->getItemsPerFirstPage() : $this->getItemsPerPage();

        if ($this->isLast()) {
            $count = $this->getItemsCount() - $this->getOffset();
        }

        return $count;
    }
}
