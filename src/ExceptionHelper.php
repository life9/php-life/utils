<?php

declare(strict_types=1);

namespace LifePHP\Utils;

class ExceptionHelper
{
    use StaticClass;

    /**
     * This method gets last error message and delete name of function which throwed error
     *
     * @param  bool $escapeHtml When it's true, this method will convert HTML5 in message to plain text
     * @return string
     */
    public static function getLastError(bool $escapeHtml = false): string
    {
        $message = error_get_last()['message'] ?? '';
        $message = $escapeHtml ? Html::htmlToText($message) : $message;
        return (string) preg_replace('~^\w+\(.*?\): ~', '', $message);
    }
}
