<?php

declare(strict_types=1);

namespace LifePHP\Utils\Exceptions;

use Exception;

class InvalidArgumentException extends Exception
{
}
