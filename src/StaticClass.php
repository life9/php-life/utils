<?php

declare(strict_types=1);

namespace LifePHP\Utils;

use LifePHP\Utils\Exceptions\MemberAccessException;

trait StaticClass
{
    /**
     * @throws MemberAccessException
     */
    final protected function __construct()
    {
        throw new MemberAccessException('Class ' . static::class . ' is static annot and cannot be instantiated.');
    }

    /**
     * @param array<mixed> $data
     * @throws MemberAccessException
     */
    final public function __unserialize(array $data): never
    {
        throw new MemberAccessException('Deserialization of ' . static::class . ' class is not allowed.');
    }
}
