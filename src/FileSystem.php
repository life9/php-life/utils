<?php

declare(strict_types=1);

namespace LifePHP\Utils;

use Closure;
use Exception;
use LifePHP\Utils\Exceptions\IOException;
use LifePHP\Utils\Exceptions\RuntimeException;

class FileSystem
{
    use StaticClass;

    private const SECONDARY_EXTENSIONS = [
        'jpeg' => 'jpg',
        'aac' => 'adt',
        'adts' => 'adt',
        'aifc' => 'aif',
        'aiff' => 'aif',
        'htm' => 'html',
        'midi' => 'mid',
        'tiff' => 'tif',
        'wms' => 'wmz',
        'wp5' => 'wpd',
        'jav' => 'java',
        'php3' => 'php',
        'php4' => 'php',
        'rsls' => 'rsl',
        'rslf' => 'rsl',
        'tz2' => 'tbz',
        'tb2' => 'tbz',
        'tbz2' => 'tbz',
        'cgi' => 'pl',
    ];

    /**
     * This method normalizing path but doesn't verify if path is correct.
     * String '/../' is invalid path but this method let it intact.
     * Please check if path is correct first
     * Normalize slashes to system slashes
     */
    public static function normalizePath(string $path): string
    {
        $retVal = str_replace(['\\', '/./'], '/', $path);
        $retVal = (string) preg_replace('~(/(?!\.\./)[^/]+(?:/|(?1))+\.\.(?=/))+~', '', $retVal);
        $retVal = str_replace('/', DIRECTORY_SEPARATOR, $retVal);
        return str_replace(':\\\\', '://', $retVal); // protocol://
    }

    /**
     * This method returns normalized file extensions, extension will be in lower case,
     * and if there are multiple forms of same extension (for example jpg and JPEG)
     * this method will return just one of them (for example jpg)
     */
    public static function normalizeFileExtension(string $fileExtension): string
    {
        $fileExtension = strtolower($fileExtension);

        return self::SECONDARY_EXTENSIONS[$fileExtension] ?? $fileExtension;
    }

    /**
     * This method will create dir, if it doesn't already exist or if it isn't a file.
     * This method is using atomic operations to create dir.
     *
     * @throws IOException
     */
    public static function createDir(string $dir, int $mode = 0777): void
    {
        if (is_dir($dir)) {
            return;
        }

        if (is_file($dir)) {
            throw new IOException(
                sprintf('Unable to create dir due to same name as file: %s.', self::normalizePath($dir)),
                2,
            );
        }

        $isDirCreated = @mkdir($dir, $mode, true);

        if (!$isDirCreated && !is_dir($dir)) {
            throw new IOException(
                sprintf('Unable to create dir: %s. %s', self::normalizePath($dir), ExceptionHelper::getLastError()),
                3,
            );
        }
    }

    /**
     * This method changes file mode.
     *
     * @see    https://www.php.net/manual/en/function.chmod.php
     * @throws IOException
     */
    public static function chmod(string $file, int $mode): void
    {
        if (@chmod($file, $mode)) {
            return;
        }

        throw new IOException(
            sprintf('Unable to change mode: %s. %s', self::normalizePath($file), ExceptionHelper::getLastError()),
            4,
        );
    }

    public static function createAtomic(
        string|FileInfo $file,
        string|Closure $generator,
        bool $refreshOpcache = false,
    ): void
    {
        if (is_string($file)) {
            $file = new FileInfo($file);
        }

        if ($file->isFile()) {
            return;
        }

        $lock = self::acquireLock($file, LOCK_EX);

        // File could be created by other PHP process
        // And if it was, we assume that the content was the same when it was first created.
        if ($file->isFile()) {
            self::freeLock($lock);
            return;
        }

        if ($generator instanceof Closure) {
            $generator = $generator();
        }

        try {
            $file->write($generator);
        } catch (Exception $e) {
            throw $e;
        } finally {
            self::freeLock($lock);
        }

        if ($refreshOpcache && function_exists('opcache_invalidate')) {
            @opcache_invalidate($file->getPathname(), true); // @ can be restricted
        }
    }

    public static function saveAtomic(
        string|FileInfo $file,
        string|Closure $generator,
        bool $refreshOpcache = false,
    ): void
    {
        if (is_string($file)) {
            $file = new FileInfo($file);
        }

        if (!$file->isFile()) {
            self::createAtomic($file, $generator, $refreshOpcache);
            return;
        }

        $lock = self::acquireLock($file, LOCK_EX);

        if ($generator instanceof Closure) {
            $generator = $generator();
        }

        try {
            $file->write($generator);
        } catch (Exception $e) {
            throw $e;
        } finally {
            self::freeLock($lock);
        }

        if ($refreshOpcache && function_exists('opcache_invalidate')) {
            @opcache_invalidate($file->getPathname(), true); // @ can be restricted
        }
    }

    public static function loadAtomic(string|FileInfo $file, null|string|Closure $generator = null): null|string
    {
        if (is_string($file)) {
            $file = new FileInfo($file);
        }

        if (!$file->isFile()) {
            if ($generator === null) {
                return null;
            }
            self::createAtomic($file, $generator);
        }

        $lock = self::acquireLock($file, LOCK_SH);

        try {
            $retVal = $file->read();
        } catch (Exception $e) {
            throw $e;
        } finally {
            self::freeLock($lock);
        }

        return $retVal;
    }

    public static function requireAtomic(string|FileInfo $file, string|Closure $generator): void
    {
        if (is_string($file)) {
            $file = new FileInfo($file);
        }

        if ($file->getNormalizedExtension() !== 'php') {
            throw new RuntimeException('File to include needs to be PHP file, \'' . $file->getExtension() . '\' provided');
        }

        if (!$file->isFile()) {
            self::createAtomic($file, $generator, true);
        }

        $lock = self::acquireLock($file, LOCK_SH);

        try {
            require_once $file->getPathname();
        } catch (Exception $e) {
            throw $e;
        } finally {
            self::freeLock($lock);
        }
    }

    /**
     * @return resource
     */
    private static function acquireLock(FileInfo $file, int $mode)
    {
        $dir = $file->getPath();
        if (!is_dir($dir) && !@mkdir($dir) && !is_dir($dir)) { // @ - dir may already exist
            throw new RuntimeException("Unable to create directory '$dir'. " . error_get_last()['message']);
        }

        $file = $file->getPathname() . '.lock';

        $handle = @fopen($file, 'w'); // @ is escalated to exception
        if (!$handle) {
            throw new RuntimeException("Unable to create file '$file'. " . error_get_last()['message']);
        } elseif (!@flock($handle, $mode)) { // @ is escalated to exception
            throw new RuntimeException('Unable to acquire ' . ($mode & LOCK_EX ? 'exclusive' : 'shared') . " lock on file '$file'. " . error_get_last()['message']);
        }

        return $handle;
    }

    /**
     * @param resource|false $resource
     */
    private static function freeLock($resource): void
    {
        if ($resource) {
            @flock($resource, LOCK_UN);
            @fclose($resource);
        }
    }
}
