# LIFE PHP Utils

LIFE PHP Utils are tools for you, to make your work easier in PHP.

## Usage

Best way to install `lifephp/utils` is via [Composer](https://getcomposer.org/):

```bash
composer require lifephp/utils
```

## Documentation

Documentation can be found [here.](/.docs)

## Versions

| State  | Version | Branch | PHP   |
|--------|---------|--------|-------|
| stable | v1.1.1  | main   | >=8.1 |

## Development

These are actual code maintainers:

<a href="https://github.com/TonnyJe">
  <img alt="TonnyJe" width="80" height="80" src="https://avatars2.githubusercontent.com/u/9120518?v=3&s=80">
</a>

<a href="https://gitlab.com/Miellap">
  <img alt="Miellap" width="80" height="80" src="https://gitlab.com/uploads/-/system/user/avatar/14030435/avatar.png?width=80">
</a>
